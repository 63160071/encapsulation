/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Maintory_
 */
public class Rectangle {
    private double width;
    private double length;
    public Rectangle(double width,double length){
        this.width = width;
        this.length = length;
    }
    public double calArea(){
        double result = length * width;
        return result;
    }
    public void setWidthLength(double width,double length){
        if(width <= 0 || length <= 0){
            System.out.println("Base or Height must more than 0 !");
        }
        else if(width == length){
            System.out.println("Width and Length Must not equal !");
        }
        else{
        this.width = width;
        this.length = length;
        }
    }
}
