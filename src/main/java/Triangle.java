/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Maintory_
 */
public class Triangle {
    private double base;
    private double height;
    public Triangle(double base,double height){
        this.base = base;
        this.height = height;
    }
    public double calArea(){
        double result = 0.5 * base * height;
        return result;
    }
    public void setBaseHeight(double base,double height){
        if(base <= 0 || height <= 0){
            System.out.println("Base or Height must more than 0 !");
        }
        else{
        this.base = base;
        this.height = height;
        }
    }
}
