/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Maintory_
 */
public class Square {
    private double side;
    public Square(double side){
        this.side = side;
    }
    public double calArea(){
        double result = side * side;
        return result;
    }
    public void setSide(double side){
        if(side <= 0){
            System.out.println("Side must be more than 0 !");
        }
        else{
        this.side = side;
    }
}
