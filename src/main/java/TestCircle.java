/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Maintory_
 */
public class TestCircle {
    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        System.out.println("Area of circle1 is " + circle1.calArea());
        circle1.setR(2);
        System.out.println("Area of circle1 is " + circle1.calArea());
        circle1.setR(0);
        System.out.println("Area of circle1 is " + circle1.calArea());
         circle1.setR(300);
        System.out.println("Area of circle1 is " + circle1.calArea());
        Square square1 = new Square(4);
        System.out.println("Area of square1 is " +square1.calArea());
        square1.setSide(300);
        System.out.println("Area of square1 is " +square1.calArea());
        square1.setSide(0);
        Triangle triangle1 = new Triangle(3,4);
        System.out.println("Area of triangle1 is " +triangle1.calArea());
        triangle1.setBaseHeight(20,30);
        System.out.println("Area of square1 is " +triangle1.calArea());
        triangle1.setBaseHeight(20,30);
        triangle1.setBaseHeight(0,30);
        Rectangle rectangle1 = new Rectangle(4,5);
        System.out.println("Area of rectangle1 is " +rectangle1.calArea());
        rectangle1.setWidthLength(5,5);
        System.out.println("Area of rectangle1 is " +rectangle1.calArea());
        rectangle1.setWidthLength(10,30);
        System.out.println("Area of rectangle1 is " +rectangle1.calArea());
        rectangle1.setWidthLength(0,30);
    }
}
